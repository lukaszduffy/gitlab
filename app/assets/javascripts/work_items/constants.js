import { s__ } from '~/locale';

export const i18n = {
  fetchError: s__('WorkItem|Something went wrong when fetching the work item. Please try again.'),
  updateError: s__('WorkItem|Something went wrong while updating the work item. Please try again.'),
};
